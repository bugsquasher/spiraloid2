


#include "Spirals.hpp"
#include "Display.hpp"



#include "Program.hpp"



#include <cstdio>



int main(int argc , char** argv) {
   
   (void)argc;
   (void)argv;

   Program proggie;
   
   int ret = proggie.Init();
   if (ret != 0) {
      return ret;
   }

   proggie.Run();
   
   return 0;
}

